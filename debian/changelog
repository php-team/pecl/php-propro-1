php-propro-1 (1:1.0.2-1) unstable; urgency=medium

  * New upstream version 1.0.2
  * Split the source package for PHP 5.x

 -- Ondřej Surý <ondrej@debian.org>  Mon, 12 Jun 2023 16:43:44 +0200

php-propro (2.1.0+1.0.2+nophp8-15) unstable; urgency=medium

  * Regenerate d/control for PHP 8.2

 -- Ondřej Surý <ondrej@debian.org>  Fri, 09 Dec 2022 14:00:06 +0100

php-propro (2.1.0+1.0.2+nophp8-14) unstable; urgency=medium

  * Fix the d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 15:01:31 +0100

php-propro (2.1.0+1.0.2+nophp8-13) unstable; urgency=medium

  * Remove PHP default version override

 -- Ondřej Surý <ondrej@debian.org>  Mon, 03 Jan 2022 14:12:55 +0100

php-propro (2.1.0+1.0.2+nophp8-12) unstable; urgency=medium

  * Regenerate d/control for PHP 8.1

 -- Ondřej Surý <ondrej@debian.org>  Sat, 01 Jan 2022 13:09:55 +0100

php-propro (2.1.0+1.0.2+nophp8-10) unstable; urgency=medium

  * Recompile for PHP 7.4 until the transition is complete

 -- Ondřej Surý <ondrej@debian.org>  Fri, 26 Nov 2021 11:25:59 +0100

php-propro (2.1.0+1.0.2+nophp8-9) unstable; urgency=medium

  * Update the packaging to dh-php >= 4~

 -- Ondřej Surý <ondrej@debian.org>  Wed, 10 Nov 2021 12:01:03 +0100

php-propro (2.1.0+1.0.2+nophp8-8) unstable; urgency=medium

  * Bump B-D to dh-php >= 3.1~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 18:07:44 +0100

php-propro (2.1.0+1.0.2+nophp8-7) unstable; urgency=medium

  * Override the PHP_DEFAULT_VERSION

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 17:08:06 +0100

php-propro (2.1.0+1.0.2+nophp8-6) unstable; urgency=medium

  * Revert arch:all change, as it breaks shlibs:Depends

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 13:21:43 +0100

php-propro (2.1.0+1.0.2+nophp8-5) unstable; urgency=medium

  * The main dummy package is arch:all
  * Bump dh-php Build-Depends to >= 3.0~

 -- Ondřej Surý <ondrej@debian.org>  Sat, 20 Feb 2021 07:51:14 +0100

php-propro (2.1.0+1.0.2+nophp8-4) unstable; urgency=medium

  * Sync the changelog with Debian bullseye
  * Update d/gbp.conf for debian/main branch
  * Update standards version to 4.5.1 (no change)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Feb 2021 12:10:59 +0100

php-propro (2.1.0+1.0.2+nophp8-3) unstable; urgency=medium

  * Fix the PHP 8.0 workaround patch
  * Update d/watch to use https://pecl.php.net
  * Lower the dh-php dependency to Debian sid version
  * Update d/gbp.conf for Debian bullseye

 -- Ondřej Surý <ondrej@debian.org>  Sun, 14 Feb 2021 16:40:47 +0100

php-propro (2.1.0+1.0.2+nophp8-2) unstable; urgency=medium

  * Limit the version to 7.0-7.99 in the package-7.xml

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 21:25:54 +0200

php-propro (2.1.0+1.0.2+nophp8-1) unstable; urgency=medium

  * New upstream version 2.1.0+1.0.2+nophp8

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 20:38:16 +0200

php-propro (2.1.0+1.0.2-5) unstable; urgency=medium

  * Add rough workaround to compile with PHP 8.0

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 20:31:00 +0200

php-propro (2.1.0+1.0.2-4) unstable; urgency=medium

  * Finish conversion to debhelper compat level 10
  * Update for dh-php >= 2.0 support

 -- Ondřej Surý <ondrej@debian.org>  Sun, 11 Oct 2020 14:47:50 +0200

php-propro (2.1.0+1.0.2-3) unstable; urgency=medium

  * No change rebuild for Debian buster

 -- Ondřej Surý <ondrej@debian.org>  Wed, 28 Aug 2019 09:21:15 +0200

php-propro (2.1.0+1.0.2-2) unstable; urgency=medium

  * Bump the dependency on dh-php to >= 0.33

 -- Ondřej Surý <ondrej@debian.org>  Tue, 16 Oct 2018 10:24:26 +0000

php-propro (2.1.0+1.0.2-1) unstable; urgency=medium

  * Update Vcs-* to salsa.d.o
  * Update maintainer email to team+php-pecl@tracker.debian.org
    (Closes: #899647)
  * New upstream version 2.1.0+1.0.2

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Aug 2018 13:03:06 +0000

php-propro (2.0.1+1.0.2-1) unstable; urgency=medium

  * Imported Upstream version 2.0.1+1.0.2

 -- Ondřej Surý <ondrej@debian.org>  Mon, 29 Aug 2016 12:39:52 +0200

php-propro (2.0.0+1.0.2-2) unstable; urgency=medium

  * Move the default makefile snippet to dh-php and use a simple d/rules
    with dh-php >= 0.12~

 -- Ondřej Surý <ondrej@debian.org>  Fri, 29 Apr 2016 11:21:21 +0200

php-propro (2.0.0+1.0.2-1) unstable; urgency=medium

  * Imported Upstream version 2.0.0+1.0.2
  * Improve d/rules so it can be used with both single or multiple PECL
    upstream versions, controlled by looking for package-MAJOR.MINOR.xml,
    package-MAJOR.xml and package.xml in this order

 -- Ondřej Surý <ondrej@debian.org>  Thu, 28 Apr 2016 17:03:38 +0200

php-propro (2.0.0-1) unstable; urgency=medium

  * Move the package under Debian PHP PECL Maintainers umbrella

 -- Ondřej Surý <ondrej@debian.org>  Wed, 06 Apr 2016 19:52:35 +0200

php-propro (2.0.0-0.1) unstable; urgency=medium

  * Non-maintainer upload
  * PHP PECL Team upload
  * Update packaging for PHP 7.0 based on Nish's work in Ubuntu
   + Kill php-propro-dev (we don't need one-file packages)
   + Properly use dh_php to enable the package (Closes: #782577)

 -- Ondřej Surý <ondrej@debian.org>  Tue, 05 Apr 2016 12:12:25 +0200

php-propro (2.0.0-0ubuntu1) xenial; urgency=medium

  * New upstream release.  LP: #1564566.

 -- Nishanth Aravamudan <nish.aravamudan@canonical.com>  Thu, 31 Mar 2016 11:57:07 -0700

php-propro (1.0.0-1build1) vivid; urgency=medium

  * No change rebuild against PHP 5.6 (phpapi-20131226).

 -- Robie Basak <robie.basak@ubuntu.com>  Tue, 27 Jan 2015 15:58:01 +0000

php-propro (1.0.0-1) unstable; urgency=low

  * Initial Release (Closes: #737237).
  * This dupload has been sponsored by OLX Inc.

 -- Facundo Guerrero <guerremdq@gmail.com>  Wed, 29 Jan 2014 21:41:58 -0300
